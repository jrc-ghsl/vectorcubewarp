# -*- coding: utf-8 -*-
"""
Created on Mon Jun 12 12:16:29 2023
VectorCubeWarp - a vector-based gridded data cube warping tool using areal interpolation.
Useful for volume-preserving warping of time series of gridded data, e.g., measuring built-up surface, volume, or population per grid cell.
The method can be applied to other types of data cubes as well.
@author: Johannes H. Uhl, Luca Maffenini, Unit E.1, Joint Research Centre, European Commission, Ispra (Italy).
"""

import rasterio
from rasterio.features import shapes
from shapely.geometry import shape
import geopandas as gp
from pandas import DataFrame
import numpy as np
import subprocess
import os
import pandas as pd
import matplotlib.pyplot as plt
from shapely.geometry import Polygon
from multiprocessing.pool import Pool
import datetime

### global variables #######################################
source_crs_proj4 = '+proj=moll +lon_0=0 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs +type=crs' #proj4 string of source CRS
years=np.arange(2020,1974,-5) #we go backwards so we read the most recent epoch first, and if that is empty, we skip the tile. May be changed depending on nature of data.
############################################################

def extract_subset(bbox,in_tiff,subset_tiff):
    gdal_cmd = 'gdal_translate -of GTiff -projwin %s %s %s %s "%s" "%s"' % (bbox[0],bbox[3],bbox[2],bbox[1],in_tiff,subset_tiff)
    subprocess.check_output(gdal_cmd, shell=True)
    
def ts_areal_interpol(groupby_tuple):        
    (currtile,gdf_target_tile,gdf_source_clipped)=groupby_tuple
    gdf_target_tile_merged = gdf_source_clipped.overlay(gdf_target_tile, how='union')
    gdf_target_tile_merged = gdf_target_tile_merged.dropna(subset=['target_idx_x'])  # remove segments that do not overlap
    gdf_target_tile_merged['area_partcell_source']=gdf_target_tile_merged.area
    gdf_target_tile_merged['area_percentage_source']=100*np.divide(gdf_target_tile_merged['area_partcell_source'],gdf_target_tile_merged['source_area_fullcell_source'])
    for year in years:
        ## redistribute values based on area proportion. this is the resampling module. can be adjusted/modified as desired.
        gdf_target_tile_merged['target_values_%s' %year]=gdf_target_tile_merged['source_values_%s' %year]*gdf_target_tile_merged['area_percentage_source']/100
    gdf_target_tile_aggreg_allyrs=gdf_target_tile[['target_idx_x', 'target_idx_y','geometry']]
    for year in years:
        gdf_target_tile_aggreg = gdf_target_tile_merged.groupby(['target_idx_x','target_idx_y'])['target_values_%s' %year].sum().map(int).reset_index()
        gdf_target_tile_aggreg_allyrs=gdf_target_tile_aggreg_allyrs.merge(gdf_target_tile_aggreg,on=['target_idx_x','target_idx_y'])
    export_values=gdf_target_tile_aggreg_allyrs[['target_idx_x', 'target_idx_y']+['target_values_%s' %x for x in years]]
    return export_values

def reproj_chunk(indf,target_crs=source_crs_proj4):
    return indf.to_crs(target_crs)

def add_centroid_coords_chunk(indf):
    centr = indf.geometry.centroid
    indf['source_centr_x_source']=centr.x
    indf['source_centr_y_source']=centr.y   
    return indf

def main():
    ############################################################
    source_tif_template = './input/GHS-BUILT-S-100m_Ispra_YYYY.tif'
    target_grid_example_tif = './input/target_grid_europe_RP10_filled_depth_template.tif' #Source: Baugh et al. (2024), https://doi.org/10.2905/1D128B6C-A4EE-4858-9E34-6210707F3C81
    datadir= './output'
    visualize_outputs=False
    num_threads_reproject=2 # N parallel processes for vector cell reprojection
    num_threads_overlay=2 # N parallel processes for vector overlay and areal interpolation
    num_threads_centroid_calc=2 # N parallel processes for centroid calculation
    tilesize=500 # defines block of cells to be processed in a chunk, measured in numbers of target grid cell sizes. 
    # In the case of the River flood hazard data used in this example, 500x500 cells correspond to approx. 45x45km. 
    # In an operational environment, it is recommended to use a small tilesize (e.g. 1x1km) 
    # in a thread, and to run a few hundred threads in parallel.
    # The tilesize is optimally nesting within the overall dimensions of the target grid.
    verbose = True #  if True, will output exact timing of individual processing steps.
    ############################################################
    
    #### target grid definition
    with rasterio.open(target_grid_example_tif) as templ:
        target_crs_def = templ.crs
        dim_x_target = templ.width
        dim_y_target = templ.height  
        px_sixe_x_target,px_sixe_y_target = templ.res
        origin_x_target,origin_y_target = templ.transform * (0, templ.height)
        
    #### construct linspaces corresponding to the target grid
    target_grid_xmin=origin_x_target
    target_grid_xmax=origin_x_target+dim_x_target*px_sixe_x_target
    target_grid_ymax=origin_y_target
    target_grid_ymin=origin_y_target+dim_y_target*px_sixe_y_target
    target_linspace_x = np.linspace(target_grid_xmin,target_grid_xmax,dim_x_target)
    target_linspace_y = np.linspace(target_grid_ymin,target_grid_ymax,dim_y_target)
    
    #### loop across linspaces, and process each block of cells (defined by tilesize parameter)
    for i_lat,lat in enumerate(target_linspace_y):
        if i_lat%tilesize!=0:
            continue
        for i_lon,lon in enumerate(target_linspace_x):
            if i_lon%tilesize!=0:
                continue 
            starttime=datetime.datetime.now()
            
            #### some temp files, will be overwritten in each loop:
            curr_source_sourcegrid = datadir+os.sep+'source_raster.tif'
            target_raster_template = datadir+os.sep+'target_raster_template.tif'

            #### construct bounding boxes in source and target grid            
            latmin=lat
            latmax=lat-tilesize*px_sixe_y_target
            lonmin=lon
            lonmax=lon+tilesize*px_sixe_x_target
            latmin_int=np.round(latmin,4)
            lonmin_int=np.round(lonmin,4)
            print(datetime.datetime.now(),latmin_int,lonmin_int)
            tile_name='%s_%s' %(latmin_int,lonmin_int)    
            bbox_target = [(lonmin, latmin),(lonmin, latmax),(lonmax, latmax),(lonmax, latmin),(lonmin, latmin)]
            bbox_target_poly = Polygon(bbox_target)
            bbox_target_gdf = gp.GeoDataFrame(index=[0], crs=target_crs_def, geometry=[bbox_target_poly])
            bbox_target = bbox_target_gdf.total_bounds
            #### use a buffer to ensure that source bbox fully encloses target bbox
            bbox_source = bbox_target_gdf.to_crs(source_crs_proj4).buffer(200).total_bounds 
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'creating source vectors and appending multi-temporal input data...')   
            
            tile_is_empty=False
            #### loop over the temporal or z axis of data stack
            for year in years:               
                #### extract subset of the source data
                curr_source=source_tif_template.replace('YYYY',str(year)) 
                if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'fetching source raster data %s' %year)                
                extract_subset(bbox_source,curr_source,curr_source_sourcegrid)                       
                #### we produce vector cells of source data, append the cell values of the full data cube as attributes
                if year==years[0]:
                    #### in the first loop, we create the source grid as vector data
                    if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'creating source vectors...',year)   
                    with rasterio.open(curr_source_sourcegrid) as src:
                        px_sixe_x_source,px_sixe_y_source = src.res
                        data = src.read(1) 
                        data[data == src.nodata] = 0
                        if not data.any():
                            tile_is_empty=True
                            break # if empty contemporarily, we skip the tile                            
                        #chessboard array:
                        data_fake = np.zeros(data.shape, dtype=int)
                        data_fake[1::2, ::2] = 1
                        data_fake[::2, 1::2] = 1              
                        # Use a generator instead of a list
                        shape_gen = ((shape(s), v) for s, v in shapes(data_fake.astype(np.int16), transform=src.transform)) #shape_gen = ((shape(s), v) for s, v in shapes(data_fake.astype(int), transform=src.transform))
                        # either build a pd.DataFrame
                        df = DataFrame(shape_gen, columns=['geometry', 'class'])
                        idx_arrays=np.indices(data.shape)
                        df['source_idx_x'] = idx_arrays[1].flatten()
                        df['source_idx_y'] = idx_arrays[0].flatten()  
                        df['source_values_%s' %year] = data.flatten()
                        gdf_source = gp.GeoDataFrame(df[['source_idx_x','source_idx_y','source_values_%s' %year]], geometry=df.geometry, crs=src.crs)
                else:
                    #### in other years, we append source cell values to the cell geometries
                    with rasterio.open(curr_source_sourcegrid) as src:
                        data = src.read(1, masked=True)
                        gdf_source['source_values_%s' %year] = data.flatten()
                        
            #### catch empty tiles
            if tile_is_empty:
                print('tile is empty')
                continue
            
            #### calculate source cell area
            gdf_source['source_area_fullcell_source']=np.abs(px_sixe_x_source*px_sixe_y_source)           
            #### produce target grid as vector data           
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'fetching raster data to create target grid...')
            extract_subset(bbox_target,target_grid_example_tif,target_raster_template) ## we need target_raster_template for the export of the results            
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'done')
            ### create target vector
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'creating target vectors...') 
            with rasterio.open(target_raster_template) as src:
                data = src.read(1, masked=True)
                #chessboard array:
                data_fake = np.zeros(data.shape, dtype=int)
                data_fake[1::2, ::2] = 1
                data_fake[::2, 1::2] = 1              
                shape_gen = ((shape(s), v) for s, v in shapes(data_fake.astype(np.int16), transform=src.transform)) #shape_gen = ((shape(s), v) for s, v in shapes(data_fake.astype(int), transform=src.transform))
                df = DataFrame(shape_gen, columns=['geometry', 'class'])
                idx_arrays=np.indices(data.shape)
                df['target_idx_x'] = idx_arrays[1].flatten()
                df['target_idx_y'] = idx_arrays[0].flatten()  
                gdf_target = gp.GeoDataFrame(df[['target_idx_x','target_idx_y']], geometry=df.geometry, crs=src.crs)               

            #### reproject target vectors into source grid, parallelizing gp.to_crs():
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'reprojecting target vectors into target CRS...')    
            target_geom_source_crs_chunk_list = np.array_split(gdf_target, num_threads_reproject)
            with Pool(num_threads_reproject) as pool:
                result = pool.map(reproj_chunk, target_geom_source_crs_chunk_list)              
            gdf_target = pd.concat(result)
            del result, target_geom_source_crs_chunk_list            
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'reprojecting target vectors into target CRS done.') 
            
            #### calculate area of projected source cells
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'calculating area...') 
            gdf_target['target_area_fullcell_source']=gdf_target.area 
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'calculating area done.') 
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'created target vectors.')
            
            #### extract centroid coordinates of projected cells, used for joining, parallelizing gp.geometry.centroid:  
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'extracting source cell centroids for faster spatial join...')
            gdf_source_chunk_list = np.array_split(gdf_source, num_threads_centroid_calc)
            with Pool(num_threads_centroid_calc) as pool:
                result = pool.map(add_centroid_coords_chunk, gdf_source_chunk_list)              
            gdf_source = pd.concat(result)
            del result, gdf_source_chunk_list                                  
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'extracting source cell centroids for faster spatial join done.')
            
            ### create a handcrafted spatial index used for parallelizing
            gdf_target['target_idx_x_bin']=pd.cut(gdf_target.target_idx_x,100,labels=False)
            gdf_target['target_idx_y_bin']=pd.cut(gdf_target.target_idx_y,100,labels=False)
                        
            ### partition target cells into chunks
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'partitioning target vector data...')
            tasks = gdf_target.groupby(['target_idx_x_bin','target_idx_y_bin'])
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'partitioning source vector data...')
            tasks_list = [(currtile,group) for currtile,group in tasks]
            tasks_list_complete=[]
            ### for each chunk of target cells, fetch the source cells that overlap and store them in list.
            for taskcount,task in enumerate(tasks_list):
                gdf_target_tile=task[1]
                bbox_target = gdf_target_tile.total_bounds
                #get subset of source df for tile, fastest:
                gdf_source_clipped = gdf_source[np.logical_and(gdf_source.source_centr_x_source>bbox_target[0]-100,gdf_source.source_centr_x_source<bbox_target[2]+100)]  
                gdf_source_clipped = gdf_source_clipped[np.logical_and(gdf_source_clipped.source_centr_y_source>bbox_target[1]-100,gdf_source_clipped.source_centr_y_source<bbox_target[3]+100)] 
                if gdf_source_clipped['source_values_%s' %years[0]].sum()==0:
                    continue                    
                else:
                    tasks_list_complete.append((task[0],task[1],gdf_source_clipped))
            del tasks,tasks_list
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'partitioning source and target vector data done.')
            
            #### parallel vector overlay and time series disaggregation from source cells into target cells using "pool" - free from GIL constraint
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'start parallel vector overlay and time series disaggregation')   
            with Pool(num_threads_overlay) as pool:
                result = pool.map(ts_areal_interpol, tasks_list_complete)                    
            if not result:
                # result is empty, continue
                print('result is empty!')  
                continue
            alltile_results = pd.concat(result, axis=1)
            del result, tasks_list_complete
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'end parallel vector overlay and time series disaggregation')   
             
            ##### restructure the output
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'restructure output')  
            outdata_restruct=[]
            allcols=alltile_results.columns
            segments=int(allcols.shape[0]/(len(years)+2))
            for i,row in alltile_results.iterrows():
                row_restruct = np.reshape(row.values,(segments,len(years)+2)) 
                for ii in np.arange(row_restruct.shape[0]):
                    outdata_restruct.append(list(row_restruct[ii,:]))
            outdata_restruct_df=pd.DataFrame(outdata_restruct,columns=['target_idx_x', 'target_idx_y']+['target_values_%s' %x for x in years])
            del alltile_results
            
            #### convert populated target cells back into raster data for each z of the data cube and write to disk
            outdata_restruct_df = outdata_restruct_df.dropna()
            outdata_restruct_df.target_idx_x = outdata_restruct_df.target_idx_x.map(int)
            outdata_restruct_df.target_idx_y = outdata_restruct_df.target_idx_y.map(int)
            if verbose: print(datetime.datetime.now(),latmin_int,lonmin_int,'export output')  
            for year in years:
                outarr = np.zeros((tilesize,tilesize))
                outarr[outdata_restruct_df.target_idx_y.values,outdata_restruct_df.target_idx_x.values]=outdata_restruct_df['target_values_%s' %year].values
                if visualize_outputs:
                    plt.imshow(np.flipud(outarr),vmin=0,vmax=500,interpolation=None)
                    plt.title(year)
                    plt.show()
                target_raster_export = datadir + os.sep + 'target_data_areal_interpol_%s_%s.tif' %(tile_name,year)
                with rasterio.open(target_raster_template) as dest:
                    ras_meta_dest = dest.profile
                    with rasterio.open(curr_source_sourcegrid) as src:
                        ras_meta_source = src.profile
                        source_nan = src.nodata 
                        source_dtype = ras_meta_source['dtype']                  
                    ras_meta_dest.update(
                        count=1,
                        compress='lzw',
                        dtype=source_dtype,
                        nodata=source_nan)
                with rasterio.open(target_raster_export, 'w', **ras_meta_dest) as dst:
                    dst.nodata=source_nan
                    dst.write(np.expand_dims(outarr,0))            
                src.close()
                dst.close()   
                print(datetime.datetime.now(),latmin_int,lonmin_int,'exported %s' %year)    
            del outdata_restruct_df, outarr
            endtime = datetime.datetime.now()
            delta = endtime - starttime
            print(datetime.datetime.now(),latmin_int,lonmin_int,'tile processing duration [seconds]:',delta.total_seconds())
            #####################################################################################
                
if __name__ == '__main__':
    main()   
    print('done.')     
        