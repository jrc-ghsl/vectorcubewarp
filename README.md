# VectorCubeWarp

A Python-based tool for warping gridded spatio-temporal data cubes between different grid definitions / spatial reference systems, using area-weighted interpolation, preserving the total sum of grid cell values per cross-section and across the whole data cube. VectorCubeWarp is intended for incorporation into large-scale parallel processing workflows.

## Description
The conversion of gridded data between two grids and/or spatial reference systems, commonly referred to as “warping”, is a fundamental step in geospatial data processing workflows. This process involves data resampling, which inherently introduces uncertainty. When dealing with stacks of statistical, gridded datasets measuring cell-level densities, consistently enumerated across multiple points in time, it is crucial to employ a volume-preserving method. Such a method should not only preserve changes in observations along the temporal dimension but also maintain the total sums of measured data per point in time, and allowing for different resampling strategies, while minimizing local distortions in the warped data. Conventional raster-based warping tools available in Geographic Information Systems and coding-based geospatial data processing environments lack explicit control over these critical properties. To address this limitation, we propose a novel vector-based method for areal interpolation based on spatial overlay operations. This approach enables lossless resampling of gridded data, which we apply to the gridded built-up surface data from the Global Human Settlement Layer (GHSL) covering the period from 1975 to 2030. As vector-based spatial data operations are computationally expensive, our method leverages a parallel-processing framework, allowing efficient warping of global gridded data cubes. Furthermore, this approach facilitates the provision of statistical data cubes across various spatial reference systems and grid definitions at planetary scale and high spatial resolution, extendible to the use of areal or spatio-temporal interpolation methods. We implemented this method in Python and call it “VectorCubeWarp”.

![GHSL global built-up surface dataset warped from World Mollweide into WGS84](./VectorCubeWarp.png)

## Installation
No installation required. The script VectorCubeWarp_Example.py can be run in any Python environment (tested in Python 3.9 only). Dependencies (i.e., rasterio, shapely, geopandas, pandas, numpy) must be available.

## Usage
VectorCubeWarp can be incorporated into large-scale processing workflows, optimally using parallelisation. It is recommended to partition the data into tiles, and process each tile in a separate thread. Subsequently, output data can be mosaicked together.

## Toy example
The data provided in this repository allows for testing VectorCubeWarp to resample a data cube of built-up surface estimates per 100x100m grid cell (ESRI:54009; World Mollweide Equal Area Projection), in 5-year intervals from 1975 to 2020, to a grid of 3 arcseconds in WGS84, as used for datasets from the FLOODS River Flood Hazard Maps project (https://doi.org/10.2905/1D128B6C-A4EE-4858-9E34-6210707F3C81). While processing the toy example may be slow on a local machine, VectorCubeWarp is highly performant when applied to small data chunks within a server-based parallel processing workflow. 

## Support
JRC-GHSL-DATA@ec.europa.eu

## Authors and acknowledgment
Johannes H. Uhl, Luca Maffenini, Unit E.1, Joint Research Centre, European Commission, 2023-2024.

## License
Licensed under the EUROPEAN UNION PUBLIC LICENCE (EUPL) v. 1.2

